#!/usr/bin/env bash

# disable check for update script
export TEMPLATE_CHECK_UPDATE_DISABLED=1

# load CF scripts from template
scripts=/tmp/cf-scripts.sh
echo "#!/bin/bash" > $scripts
sed -n '/BEGSCRIPT/,/ENDSCRIPT/p' "$(dirname ${BASH_SOURCE[0]})/../templates/gitlab-ci-cf.yml" >> $scripts
source $scripts

# define CF CLI mock
function cf() {
  echo "cf $*"
}

function ruby() {
    nb_results=${#RUBY_RESULT[@]}
    if [[ ${nb_results} -ne 0 ]]
    then
        result=${RUBY_RESULT[0]}
        RUBY_RESULT=${RUBY_RESULT[@]:1:$nb_results}
        echo "$result"
    fi
}

# export mock to make visible to child scripts
export -f cf ruby

# mock default domain
function default_domain() {
  echo "domain.intra"
}
